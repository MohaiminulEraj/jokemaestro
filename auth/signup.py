import os
import boto3
import json


# MODIFY
USER_POOL_ID = os.environ.get['USER_POOL_ID']
CLIENT_ID = os.environ.get['USER_POOL_CLIENT_ID']

client = boto3.client('cognito-idp')


def sign_up(event, context):
    username = event['body']
    data = json.loads(username)
    email = data["email"]
    password = data["password"]
    try:
        res = client.sign_up(
            ClientId=CLIENT_ID,
            Username=email,
            Password=password)

    except client.exceptions.UsernameExistsException as e:
        print(e)

    return {
        "statusCode": 200,
        "body": json.dumps({
            "message": res
        })
    }
